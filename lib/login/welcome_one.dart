import 'package:flutter/material.dart';

import 'util/utility.dart';

class WelcomeOne extends StatelessWidget {
  bool isLogin;
  WelcomeOne({Key key, this.isLogin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LayoutBuilder(builder: (context, constraint) {
      double screenHeight = constraint.maxHeight;

      return isLogin
          ? _buildLoginHome(screenHeight)
          : _buildSignupHome(screenHeight);
    }));
  }

  Widget _buildSignupHome(double height) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          height: height * 0.165,
          child: Stack(
            children: <Widget>[
              ClipPath(
                clipper: WelcomeClipper(),
                child: Container(
                  color: LoginColor.bgColor,
                ),
              ),
              Positioned(
                bottom: 20,
                right: 70,
                child: IconButton(
                  icon: Icon(Icons.close, color: LoginColor.bgColor),
                  iconSize: 30,
                  onPressed: () {},
                ),
              ),
              Positioned(
                bottom: 20,
                right: 10,
                child: IconButton(
                  icon: Icon(Icons.person, color: LoginColor.lightColor),
                  iconSize: 30,
                  onPressed: () {},
                ),
              )
            ],
          ),
        ),
        SizedBox(height: 10),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text("Yeah!",
                  style: TextStyle(color: LoginColor.darkColor, fontSize: 36)),
              Text("Signed up successfully.",
                  style: TextStyle(
                      color: LoginColor.darkColor,
                      fontSize: 20,
                      fontStyle: FontStyle.italic)),
              Text("Please check your email.",
                  style: TextStyle(
                      color: LoginColor.darkColor,
                      fontSize: 20,
                      fontStyle: FontStyle.italic)),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildLoginHome(double height) {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: WelcomeClipper(),
          child: Container(
            color: LoginColor.bgColor,
            height: height,
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 32,
                child: Icon(
                  Icons.person,
                  size: 48,
                ),
              ),
              SizedBox(height: 10),
              Text("Welcome",
                  style: TextStyle(color: Colors.white, fontSize: 36)),
              Text("Thanks for logging in",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontStyle: FontStyle.italic)),
            ],
          ),
        ),
        Positioned(
          bottom: 20,
          right: 70,
          child: IconButton(
            icon: Icon(Icons.home, color: LoginColor.bgColor),
            iconSize: 30,
            onPressed: () {},
          ),
        ),
        Positioned(
          bottom: 20,
          right: 10,
          child: IconButton(
            icon: Icon(Icons.close, color: LoginColor.lightColor),
            iconSize: 30,
            onPressed: () {},
          ),
        )
      ],
    );
  }
}
