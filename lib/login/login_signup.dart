import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_design/login/welcome_one.dart';

import 'util/utility.dart';

class LoginSignup extends StatefulWidget {
  @override
  _LoginSignupState createState() => _LoginSignupState();
}

class _LoginSignupState extends State<LoginSignup> {
  TextEditingController _emailLogin = new TextEditingController();
  TextEditingController _passwordLogin = new TextEditingController();
  TextEditingController _emailRegister = new TextEditingController();
  TextEditingController _passwordRegister = new TextEditingController();
  GlobalKey<FormState> _loginFormKey = new GlobalKey<FormState>();
  GlobalKey<FormState> _registerFormKey = new GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _rememberMe = true;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final screenHeight = screenSize.height;
    final screenWidth = screenSize.width;

    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: ClipPath(
                  clipper: LoginClipper(),
                  child: Container(
                    color: LoginColor.bgColor,
                    height: screenHeight,
                  ),
                ),
              ),
              Positioned(
                bottom: screenHeight * 0.48,
                right: screenWidth * 0.165,
                child: IconButton(
                  icon: Icon(Icons.keyboard_arrow_down,
                      color: LoginColor.bgColor),
                  iconSize: 30,
                  onPressed: () {
                    if (_emailLogin.text.isNotEmpty &&
                        _passwordLogin.text.isNotEmpty) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => WelcomeOne(isLogin: true)));
                    } else {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text("Login failed. Try again later!")));
                    }
                  },
                ),
              ),
              Positioned(
                bottom: screenHeight * 0.48,
                right: screenWidth * 0.03,
                child: IconButton(
                  icon: Icon(Icons.keyboard_arrow_up,
                      color: LoginColor.lightColor),
                  iconSize: 30,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WelcomeOne(isLogin: false)));
                    if (_emailRegister.text.isNotEmpty &&
                        _passwordRegister.text.isNotEmpty) {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text("Your registration is successful!")));
                    } else {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(
                              "User registration failed. Try again later")));
                    }
                  },
                ),
              ),
              Positioned(
                top: 40,
                height: screenHeight * 0.4,
                child: _buildLogin(screenWidth, screenHeight),
              ),
              Positioned(
                top: screenHeight * 0.55,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      height: screenHeight * 0.35,
                      child: _buildSignup(screenWidth, screenHeight)),
                ),
              )
            ],
          ),
        ));
  }

  Widget _buildSignup(double width, double height) {
    return Form(
      key: _registerFormKey,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "Signup",
                style: TextStyle(
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                    color: LoginColor.darkColor),
              ),
            ),
          ),
          _buildTextField(width, "Email Address", false, _emailRegister,
              LoginColor.darkColor),
          _buildTextField(
              width, "Password", true, _passwordRegister, LoginColor.darkColor),
          Expanded(
            child: InkWell(
                onTap: () {},
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        activeColor: LoginColor.bgColor,
                        value: this._rememberMe,
                        onChanged: (bool value) {
                          setState(() {
                            this._rememberMe = value;
                          });
                        },
                      ),
                      Text(
                        "Remember me",
                        style: TextStyle(color: Colors.grey, fontSize: 18.0),
                      ),
                    ],
                  ),
                )),
          )
        ],
      ),
    );
  }

  Widget _buildLogin(double width, double height) {
    return Form(
      key: _loginFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "Login",
                style: TextStyle(
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          _buildTextField(width, "Email Address", false, _emailLogin,
              LoginColor.lightColor),
          _buildTextField(
              width, "Password", true, _passwordLogin, LoginColor.lightColor),
          //   SizedBox(height: 10),
          Expanded(
            flex: 1,
            child: InkWell(
                onTap: () {
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text("Oh! Did you forget your password?")));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20.0),
                  child: Text(
                    "Forgot Password?",
                    style: TextStyle(color: Colors.grey, fontSize: 18.0),
                  ),
                )),
          )
        ],
      ),
    );
  }

  Widget _buildTextField(double width, String label, bool isPassword,
      TextEditingController _controller, Color textColor) {
    return Expanded(
      flex: 1,
      child: Container(
        width: width * 0.9,
        margin: const EdgeInsets.only(
          left: 20.0,
          right: 20.0,
        ),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
                color: Colors.redAccent, width: 0.5, style: BorderStyle.solid),
          ),
        ),
        padding: const EdgeInsets.only(left: 0.0, right: 10.0),
        child: TextField(
            obscureText: isPassword,
            textAlign: TextAlign.left,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(color: textColor),
            decoration: InputDecoration(
                border: InputBorder.none,
                labelText: label,
                labelStyle: TextStyle(color: textColor, fontSize: 18.0)
                // hintText: 'Email Address',
                // hintStyle: TextStyle(color: Colors.grey),
                ),
            controller: _controller),
      ),
    );
  }
}
