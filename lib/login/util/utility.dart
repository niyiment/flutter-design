import 'package:flutter/material.dart';

class LoginColor {
  static final Color bgColor = Color(0x0ff092250);
  static final Color lightBgColor = Color(0x0ffefefef);
  static final Color lightColor = Colors.white;
  static final Color darkColor = Color(0x0ff10132a);
}

class LoginClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();

    final double _height = size.height * 0.55;
    path.lineTo(0, _height - 40);
    path.lineTo(size.width - (size.width * 0.32), (_height - 40));
    path.quadraticBezierTo(size.width - (size.width * 0.24), (_height - 100),
        size.width - (size.width * 0.16), (_height - 40));

    path.quadraticBezierTo(
        size.width - (size.width * 0.08), _height, size.width, (_height - 50));
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class WelcomeClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    final double _height = size.height;
    path.lineTo(0, _height - 40);
    path.lineTo(size.width - (size.width * 0.32), (_height - 40));
    path.quadraticBezierTo(size.width - (size.width * 0.24), (_height - 100),
        size.width - (size.width * 0.16), (_height - 40));

    path.quadraticBezierTo(
        size.width - (size.width * 0.08), _height, size.width, (_height - 50));
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
