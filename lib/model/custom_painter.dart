import 'package:flutter/material.dart';

class WelcomeOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: PathPainter(),
    );
  }
}

class PathPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 8.0;

    Path path = Path();
    // Moves starting point to the center of the screen
    // path.moveTo(size.width / 2, size.height / 2);

    //Draws a line from left top corner to right bottom
    path.lineTo(size.width, size.height);
    path.close();
    canvas.drawPath(path, paint);

    //draw an arc
    // path.moveTo(0, size.height / 2);
    // path.quadraticBezierTo(
    //     size.width / 2, size.height, size.width, size.height / 2);
    // canvas.drawPath(path, paint);

//draw a wavy  line
    // path.cubicTo(size.width / 4, 3 * size.height / 4, 3 * size.width / 4,
    //     size.height / 4, size.width, size.height);
    // canvas.drawPath(path, paint);

    //curve line using conicTo
    // path.conicTo(
    //     size.width / 4, 3 * size.height / 4, size.width, size.height, 20);
    // canvas.drawPath(path, paint);

    //arc of an oval
    // Method to convert degree to radians
    // num degToRad(num deg) => deg * (Math.pi / 180.0);

    // path.arcTo(
    //     Rect.fromLTWH(
    //         size.width / 2, size.height / 2, size.width / 4, size.height / 4),
    //     degToRad(0),
    //     degToRad(90),
    //     true);
    // canvas.drawPath(path, paint);

    // Adds a rectangle
    // path.addRect(Rect.fromLTWH(
    //     size.width / 2, size.height / 2, size.width / 4, size.height / 4));
    // canvas.drawPath(path, paint);

    // Adds an oval
    // path.addOval(Rect.fromLTWH(
    //     size.width / 2, size.height / 2, size.width / 4, size.height / 4));
    // canvas.drawPath(path, paint);

    // Adds a quarter arc
    // path.addArc(Rect.fromLTWH(0, 0, size.width, size.height), degToRad(180),
    //     degToRad(90));
    // canvas.drawPath(path, paint);

    // Adds a polygon from the starting point to quarter point of the screen and lastly
    // it will be in the bottom middle. Close method will draw a line between start and end.
    // path.addPolygon([
    //   Offset.zero,
    //   Offset(size.width / 4, size.height / 4),
    //   Offset(size.width / 2, size.height)
    // ], true);
    // canvas.drawPath(path, paint);

    //rounded corner rectangle
    // path.addRRect(
    //   RRect.fromRectAndRadius(Rect.fromLTWH(size.width / 2, size.height / 2, size.width / 4, size.height / 4), Radius.circular(16))
    // );
    // canvas.drawPath(path, paint);

//addPath
    // path.addRRect(RRect.fromRectAndRadius(
    //     Rect.fromLTWH(
    //         size.width / 2, size.height / 2, size.width / 4, size.height / 4),
    //     Radius.circular(16)));
    // Path secondPath = Path();
    // secondPath.lineTo(size.width / 2, size.height / 2);
    // path.addPath(secondPath, Offset(16, 16));
    // canvas.drawPath(path, paint);

    //relaticCubicTo
    // path.moveTo(size.width / 4, size.height / 4);
    // path.relativeCubicTo(size.width / 4, 3 * size.height / 4,
    //     3 * size.width / 4, size.height / 4, size.width, size.height);
    // canvas.drawPath(path, paint);

    //relativeLineTo
    // path.moveTo(size.width / 4, size.height / 4);
    // path.relativeLineTo(size.width / 2, size.height / 2);
    // canvas.drawPath(path, paint);

    //relativeConicTo
    // path.moveTo(size.width / 4, size.height / 4);
    // path.relativeConicTo(
    //     size.width / 4, 3 * size.height / 4, size.width, size.height, 20);
    // canvas.drawPath(path, paint);

    //relativeQuadraticBezierTo
    // path.moveTo(size.width / 4, size.height / 4);
    // path.relativeQuadraticBezierTo(
    //     size.width / 2, size.height, size.width, size.height / 2);
    // canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
