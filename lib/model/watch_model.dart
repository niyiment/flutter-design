import 'dart:ui';

class WatchTheme {
  static final Color bgColor1 = Color(0x0FF545952);
  static final Color bgColor2 = Color(0x0FF87908d);
  static final Color bgColor3 = Color(0x0FFa6aaa9);
  static final Color priceColor = Color(0x0FFb37358);
  static final Color priceColor2 = Color(0x0FFf4d4bf);
  static final Color buttonColor = Color(0x0FFb37358);
  static final String watchImage = "assets/images/watch.png";
}

class Product {
  int id;
  String title;
  String subTitle;
  String description;
  String imageUrl;
  double price;
  double promoPrice;
  String bandType;
  String bandWidth;
  String bezelMaterial;
  int quantity;

  Product(
      {this.id,
      this.title,
      this.subTitle,
      this.description,
      this.imageUrl,
      this.price,
      this.bandType,
      this.bandWidth,
      this.bezelMaterial,
      this.promoPrice = 0.00,
      this.quantity = 1});
}

List<Product> products = [
  Product(
    title: "Nixon",
    subTitle: "A49527229",
    description:
        "The Nixon, C39 Leather series features a stainless steel 18mm case, with a fixed bezel, a white dial and a scratch resistant mineral crystal.",
    imageUrl: "assets/images/watch.png",
    price: 325.00,
    bandType: "Stripe",
    bandWidth: "18mm",
    bezelMaterial: "Stainless steel",
    promoPrice: 165.98,
  ),
  Product(
    title: "Nixion 2",
    subTitle: "Nixion 2",
    description:
        "The Nixion, D50 Leather series features a stainless steel 39mm case, with a fixed bezel, a white dial and a scratch resistant mineral crystal.",
    imageUrl: "assets/images/watch1.png",
    price: 275.00,
    bandType: "Stripe",
    bandWidth: "39mm",
    bezelMaterial: "Stainless steel",
    promoPrice: 145.98,
  ),
  Product(
    title: "Nixon 3",
    subTitle: "Nixon 3",
    description:
        "The Nixion, D50 Leather series features a stainless steel 39mm case, with a fixed bezel, a white dial and a scratch resistant mineral crystal.",
    imageUrl: "assets/images/watch2.png",
    price: 275.00,
    bandType: "Stripe",
    bandWidth: "39mm",
    bezelMaterial: "Stainless steel",
    promoPrice: 145.98,
  )
];
