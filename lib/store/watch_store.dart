import 'package:flutter/material.dart';
import 'package:flutter_design/store/watch_store_detail.dart';

import '../model/watch_model.dart';

class WatchStore extends StatefulWidget {
  @override
  _WatchStoreState createState() => _WatchStoreState();
}

class _WatchStoreState extends State<WatchStore> {
  double deviceHeight;
  double deviceWidth;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    deviceHeight = deviceSize.height;
    deviceWidth = deviceSize.width;

    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      height: deviceHeight * 0.8,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            WatchTheme.bgColor1,
                            WatchTheme.bgColor2,
                            WatchTheme.bgColor3
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.bottomLeft,
                        ),
                      )),
                ),
                Positioned(
                    top: 0,
                    height: deviceHeight * 0.8,
                    child: Image.asset(
                      WatchTheme.watchImage,
                      fit: BoxFit.fitHeight,
                    )),
                Positioned(
                  top: 30,
                  left: 20,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back, color: Colors.black),
                    ),
                  ),
                ),
                Positioned(
                    bottom: 40,
                    left: 20,
                    height: 250,
                    width: deviceWidth * 0.9,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      physics: ClampingScrollPhysics(),
                      itemCount: products.length,
                      itemBuilder: (context, index) {
                        return _buildListItem(context, products[index]);
                      },
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildListItem(context, Product product) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => WatchStoreDetail(product: product)));
      },
      child: Container(
        height: 220,
        width: deviceWidth * 0.8,
        margin: const EdgeInsets.all(8.0),
        child: Material(
          borderRadius: BorderRadius.circular(12.0),
          elevation: 10.0,
          shadowColor: Color(0x802196F3),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.only(left: 25.0, top: 10, right: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(product.title,
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            )),
                        Text(product.subTitle,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.grey)),
                      ],
                    ),
                    SizedBox(width: 30),
                    Icon(Icons.dashboard,
                        color: WatchTheme.priceColor, size: 40)
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 25, top: 12.0, right: 25, bottom: 12.0),
                child: Text(product.description,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    maxLines: 4,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey)),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 25, top: 12.0, right: 25, bottom: 12.0),
                child: product.promoPrice != 0
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("\$${product.promoPrice.toStringAsFixed(2)}",
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: WatchTheme.priceColor)),
                          Text("\$${product.price.toStringAsFixed(2)}",
                              style: TextStyle(
                                  decoration: TextDecoration.lineThrough,
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: WatchTheme.priceColor2)),
                        ],
                      )
                    : Text("\$${product.price.toStringAsFixed(2)}",
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: WatchTheme.priceColor)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
