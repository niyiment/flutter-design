import 'package:flutter/material.dart';

import 'furniture_detail.dart';

class FurnitureStore extends StatefulWidget {
  @override
  _FurnitureStoreState createState() => _FurnitureStoreState();
}

class _FurnitureStoreState extends State<FurnitureStore> {
  int counter = 0;
  double price = 75.00;
  //app bar widget
  Widget _buildAppBar() {
    return Padding(
        padding: const EdgeInsets.only(left: 0.0, top: 40.0, right: 20.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 60,
                  width: 65,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      color: FurnitureTheme.bgColor1),
                  child: Icon(Icons.arrow_back_ios, color: Colors.white),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Chairs",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87),
                  ),
                  Text(
                    "Elegant Furniture",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey),
                  ),
                ],
              ),
              IconButton(
                icon: Icon(Icons.favorite_border),
                hoverColor: Colors.redAccent,
                color: Colors.red,
                onPressed: () {},
              )
            ]));
  }

  //product image widget
  Widget _buildImageContainer(double height, double width) {
    return Container(
      width: width * 0.9,
      margin: const EdgeInsets.only(left: 20, right: 20),
      decoration: BoxDecoration(
          // boxShadow: [
          //   BoxShadow(
          //       color: FurnitureTheme.boxShadow,
          //       blurRadius: 0.2,
          //       offset: Offset(0.5, 0.1))
          // ],
          image: DecorationImage(
        image: ExactAssetImage(FurnitureTheme.imageUrl),
        fit: BoxFit.cover,
      )),
    );
  }

  Widget _buildTitle() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Beetle chair",
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w800,
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.grey, size: 20),
              SizedBox(width: 10),
              Text(
                "245",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              SizedBox(width: 5),
              Text(
                "Review",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.grey,
                ),
              ),
              Expanded(
                  child: Icon(Icons.arrow_drop_down_circle, color: Colors.grey))
            ],
          )
        ],
      ),
    );
  }

  Widget _buildBottomContainer(double height) {
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              gradient: LinearGradient(
                  colors: [FurnitureTheme.bgColor1, FurnitureTheme.bgColor2])),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 10.0, right: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("\$",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w800)),
                      Text("${price.truncate()}",
                          textScaleFactor: 2,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w800)),
                      Text(".00",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w800)),
                    ],
                  ),
                  Text("Price per unit", style: TextStyle(color: Colors.grey)),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        height: 25,
                        width: 25,
                        margin: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 10),
                        child: OutlineButton(
                          padding: const EdgeInsets.only(left: 2),
                          child:
                              Text("-", style: TextStyle(color: Colors.white)),
                          borderSide: BorderSide(color: Colors.white),
                          shape: BeveledRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          onPressed: () {
                            setState(() {
                              counter != 0 ? counter-- : counter = 0;
                            });
                          },
                        ),
                      ),
                      Text("$counter", style: TextStyle(color: Colors.white)),
                      Container(
                        height: 25,
                        width: 25,
                        margin: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 10),
                        child: OutlineButton(
                          padding: const EdgeInsets.only(left: 2),
                          child:
                              Text("+", style: TextStyle(color: Colors.white)),
                          borderSide: BorderSide(color: Colors.white),
                          shape: BeveledRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          onPressed: () {
                            setState(() {
                              counter++;
                            });
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FurnitureStoreDetail(
                              counter: counter, price: price)));
                },
                child: Container(
                  width: 100,
                  margin: const EdgeInsets.only(top: 30),
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.shopping_basket, color: Colors.white),
                      SizedBox(height: 20),
                      Text("Buy",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold))
                    ],
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraint) {
          var screenHeight = constraint.maxHeight;
          var screenWidth = constraint.maxWidth;

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(flex: 2, child: _buildAppBar()),
              Expanded(
                  flex: 4,
                  child: _buildImageContainer(screenHeight, screenWidth)),
              Expanded(flex: 2, child: _buildTitle()),
              Expanded(flex: 3, child: _buildBottomContainer(screenHeight))
            ],
          );
        },
      ),
    );
  }
}

class FurnitureTheme {
  static final Color bgColor1 = Color(0x0FF3e2317);
  static final Color bgColor2 = Color(0x0FF5b3f33);
  static final Color btnColor = Colors.black;
  static final Color boxShadow = Color(0x0FFf3f3f5);
  static final String imageUrl = "assets/images/chair.png";
}
