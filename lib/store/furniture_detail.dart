import 'package:flutter/material.dart';

class FurnitureStoreDetail extends StatefulWidget {
  final int counter;
  final double price;

  FurnitureStoreDetail({Key key, this.counter, this.price}) : super(key: key);

  @override
  _FurnitureStoreDetailState createState() => _FurnitureStoreDetailState();
}

class _FurnitureStoreDetailState extends State<FurnitureStoreDetail> {
  double total = 0.0;
  String digit = "0.0";

  //app bar widget
  Widget _buildAppBar(double width) {
    return Padding(
        padding: const EdgeInsets.only(left: 0.0, top: 10.0, right: 20.0),
        child: Row(children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              height: 60,
              width: 65,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                  color: FurnitureTheme.bgColor1),
              child: Icon(Icons.arrow_back_ios, color: Colors.white),
            ),
          ),
          SizedBox(width: width * 0.25),
          Text(
            "Checkout",
            style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Colors.black87),
          )
        ]));
  }

  //product image widget
  Widget _buildImageContainer(double height, double width) {
    return Stack(fit: StackFit.expand, children: [
      Align(
          alignment: Alignment.topLeft,
          child: Image.asset(
            "assets/images/chair1.png",
            width: 80,
            height: 100,
            fit: BoxFit.cover,
          )),
      Align(
          alignment: Alignment.bottomCenter,
          child: Image.asset(
            "assets/images/chair.png",
            width: 100,
            height: 200,
            fit: BoxFit.cover,
          )),
      Align(
          alignment: Alignment.topRight,
          child: Image.asset(
            "assets/images/chair2.png",
            width: 80,
            height: 100,
            fit: BoxFit.cover,
          )),
    ]);
  }

  Widget _buildTitle() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Beetle chair",
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w800,
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.yellow, size: 20),
              Icon(Icons.star, color: Colors.grey, size: 20),
              SizedBox(width: 10),
              Text(
                "245",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              SizedBox(width: 5),
              Text(
                "Review",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.grey,
                ),
              ),
              Expanded(
                  child: Icon(Icons.arrow_drop_down_circle, color: Colors.grey))
            ],
          )
        ],
      ),
    );
  }

  Widget _buildCreditCard(String title, String cardType, String cardNumber,
      String expiry, IconData icon, double width, Color color) {
    return Container(
      height: 150,
      width: width * 0.76,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
          color: Colors.brown.shade900,
          borderRadius: BorderRadius.circular(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(title,
                  style: TextStyle(
                      fontWeight: FontWeight.w600, color: Colors.white)),
              Container(
                height: 20,
                width: 20,
                margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 8),
                child: OutlineButton(
                  padding: const EdgeInsets.only(left: 2),
                  child: Text("-", style: TextStyle(color: Colors.white)),
                  borderSide: BorderSide(color: Colors.white),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                size: 24.0,
                color: color,
              ),
              Text(cardType,
                  style: TextStyle(
                      fontWeight: FontWeight.w600, color: Colors.white)),
              Text("**** $cardNumber",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w300,
                      color: Colors.white)),
            ],
          ),
          Text(expiry, style: TextStyle(fontSize: 14.0, color: Colors.grey))
        ],
      ),
    );
  }

  Widget _buildBottomContainer(double height, double width) {
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              gradient: LinearGradient(
                  colors: [FurnitureTheme.bgColor1, FurnitureTheme.bgColor2],
                  begin: Alignment.topRight,
                  end: Alignment.bottomRight)),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 10.0, right: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("\$",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w800)),
                            Text("${total.truncate()}",
                                textScaleFactor: 2,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w800)),
                            Text(".00",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w800)),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Total price",
                                style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 30),
                            CircleAvatar(
                              radius: 10,
                              backgroundColor: FurnitureTheme.bgColor2,
                              child: Text("${widget.counter}"),
                            ),
                            SizedBox(width: 5),
                            Text("Total product",
                                style: TextStyle(color: Colors.grey)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 8,
          right: 0,
          left: 20,
          child: Container(
            height: 140,
            width: width * 0.8,
            child: ListView(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                _buildCreditCard("Debit card", "Mastercard", "1659", "05/24",
                    Icons.account_balance_wallet, width, Colors.red),
                _buildCreditCard("Credit card", "Visa", "1659", "05/24",
                    Icons.account_balance_wallet, width, Colors.blue),
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Container(
            height: 60,
            width: 80,
            margin: const EdgeInsets.only(right: 20.0),
            decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.shopping_basket, color: Colors.white),
                SizedBox(height: 5),
                Text("Pay",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold))
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    total = (widget.counter > 0) ? widget.price * widget.counter : widget.price;

    print("total: $total");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LayoutBuilder(
        builder: (context, constraint) {
          var screenHeight = constraint.maxHeight;
          var screenWidth = constraint.maxWidth;

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(flex: 2, child: _buildAppBar(screenWidth)),
              Expanded(
                  flex: 2,
                  child: _buildImageContainer(screenHeight, screenWidth)),
              Expanded(flex: 1, child: _buildTitle()),
              Expanded(
                  flex: 3,
                  child: _buildBottomContainer(screenHeight, screenWidth))
            ],
          );
        },
      ),
    );
  }
}

class ImageClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0.0, size.height - 50);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 50);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class FurnitureTheme {
  static final Color bgColor1 = Color(0x0FF3e2317);
  static final Color bgColor2 = Color(0x0FF5b3f33);
  static final Color btnColor = Colors.black;
  static final Color boxShadow = Color(0x0FFf3f3f5);
  static final String imageUrl = "assets/images/chair.png";
}
