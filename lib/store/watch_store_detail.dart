import 'package:flutter/material.dart';

import '../model/watch_model.dart';

class WatchStoreDetail extends StatefulWidget {
  final Product product;
  WatchStoreDetail({Key key, @required this.product}) : super(key: key);

  @override
  _WatchStoreDetailState createState() => _WatchStoreDetailState();
}

class _WatchStoreDetailState extends State<WatchStoreDetail> {
  double deviceHeight;
  double deviceWidth;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    deviceHeight = deviceSize.height;
    deviceWidth = deviceSize.width;
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topRight,
                child: Container(
                    height: deviceHeight * 0.38,
                    width: deviceWidth * 0.85,
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(40)),
                        gradient: LinearGradient(
                          colors: [
                            WatchTheme.bgColor1,
                            WatchTheme.bgColor2,
                            WatchTheme.bgColor3
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.bottomLeft,
                        ),
                        image: DecorationImage(
                            image: ExactAssetImage(widget.product.imageUrl),
                            fit: BoxFit.cover))),
              ),
              Positioned(
                top: 30,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back,
                          color: Colors.black, size: 32)),
                ),
              ),
              // Align(
              //   alignment: Alignment.topRight,
              //   child: Container(
              //     height: deviceHeight * 0.38,
              //     width: deviceWidth * 0.85,
              //     decoration: BoxDecoration(
              //         image: DecorationImage(
              //             image: ExactAssetImage(widget.product.imageUrl),
              //             fit: BoxFit.fitWidth)),
              //   ),
              // ),
              Positioned(
                top: 80,
                left: 80,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.product.title,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      SizedBox(height: 10),
                      Text(
                        widget.product.subTitle,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey[200]),
                      )
                    ]),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 30.0, top: 10.0, right: 30.0, bottom: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                    widget.product.promoPrice != 0
                        ? "\$${widget.product.promoPrice.toStringAsFixed(2)}"
                        : "\$${widget.product.price.toStringAsFixed(2)}",
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: WatchTheme.priceColor)),
                Icon(Icons.dashboard, color: WatchTheme.priceColor, size: 40),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildRowList("Band Type", widget.product.bandType),
                  Divider(thickness: 1.2),
                  _buildRowList("Band Width", widget.product.bandWidth),
                  Divider(
                    thickness: 1.2,
                  ),
                  _buildRowList("Bezel Material", widget.product.bezelMaterial),
                  Divider(thickness: 1.2),
                ],
              )),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, top: 10, right: 30),
            child: Text(
              widget.product.description,
              style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w600,
                  color: Colors.grey),
            ),
          ),
          SizedBox(height: 20),
          InkWell(
              onTap: () {},
              child: Container(
                height: 50,
                width: deviceWidth * 0.9,
                margin: const EdgeInsets.only(
                    left: 30.0, right: 30.0, bottom: 10.0),
                child: Material(
                  borderRadius: BorderRadius.circular(12.0),
                  elevation: 5.0,
                  shadowColor: Color(0x802196F3),
                  color: WatchTheme.buttonColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "ADD TO BAG",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(width: 12.0),
                      Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 20,
                      )
                    ],
                  ),
                ),
              ))
        ],
      ),
    ));
  }

  Widget _buildRowList(String leading, String trailing) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(leading,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Colors.black)),
          Text(trailing,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Colors.grey)),
        ],
      ),
    );
  }
}
