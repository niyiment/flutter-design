# flutter_design
![Login Register](screenshot/login_register.png)
![Registration Home](screenshot/register_home.png)
![Login Home](screenshot/login_home.png)

![Watch Store](screenshot/watch_store1.png)
![Watch Store2](screenshot/watch_store2.png)

![Furniture Store1](screenshot/furniture_store1.png)
![Furniture Store2](screenshot/furniture_store2.png)

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
